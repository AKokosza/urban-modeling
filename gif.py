import os
import re
import fnmatch

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]



png_dir = "./gif/{0}/"

images = []
files=os.listdir()
first_index=max([int(re.findall('\d+', file[:-4])[0]) for file in files if fnmatch.fnmatch(file, '*.mp4')])+1

for i,name in enumerate(os.listdir(path='./gif')):
    os.system('e:\\ffmpeg\\bin\\ffmpeg.exe -r 60 -i .\\gif\\{0}\\SLEUTH%05d.png -c:v libx264 -vf "fps=60,format=yuv420p" out{1}.mp4'.format(name,i+first_index))

