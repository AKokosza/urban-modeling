# Perlin Noise Generator
# http://en.wikipedia.org/wiki/Perlin_noise
# http://en.wikipedia.org/wiki/Bilinear_interpolation
# FB36 - 20130222
import random
import math
from PIL import Image, ImageDraw
from pprint import pprint
import os

neighbourhood={(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)}
urbanised_cells=set()
cells_for_urbanisation=set()
fresh_cells=set()
def create_image():
    imgx = 910; imgy = 540 # image size
    image = Image.new("HSV", (imgx, imgy))
    draw = ImageDraw.Draw(image)
    pixels = image.load()
    octaves = int(math.log(max(imgx, imgy), 2.0))
    persistence = 0.95
    imgAr = [[0.0 for i in range(imgx)] for j in range(imgy)] # image array
    totAmp = 0.1
    for k in range(octaves):
        freq = 2 ** k
        amp = persistence ** k
        totAmp += amp
        # create an image from n by m grid of random numbers (w/ amplitude)
        # using Bilinear Interpolation
        n = freq + 1; m = freq + 1 # grid size
        ar = [[random.random() * amp for i in range(n)] for j in range(m)]
        nx = imgx / (n - 1.0); ny = imgy / (m - 1.0)
        for ky in range(imgy):
            for kx in range(imgx):
                i = int(kx / nx); j = int(ky / ny)
                dx0 = kx - i * nx; dx1 = nx - dx0
                dy0 = ky - j * ny; dy1 = ny - dy0
                z = ar[j][i] * dx1 * dy1
                z += ar[j][i + 1] * dx0 * dy1
                z += ar[j + 1][i] * dx1 * dy0
                z += ar[j + 1][i + 1] * dx0 * dy0
                z /= nx * ny
                imgAr[ky][kx] += z # add image layers together

    # paint image
    for ky in range(imgy):
        for kx in range(imgx):
            c = imgAr[ky][kx] / totAmp *1.3
            pixels[kx, ky] = (max(int(120*(1-c)),0), 180, 120)
    return image

for i in range(0,50):
    image=create_image()
    image_to_save=image.convert('RGB')
    try:
        os.makedirs('E:\\city\\terrain2')
    except:
        pass
    image_to_save.save("E:\\city\\terrain2\\{}.png".format(i), "PNG")
