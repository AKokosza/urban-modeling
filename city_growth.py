# Perlin Noise Generator
# http://en.wikipedia.org/wiki/Perlin_noise
# http://en.wikipedia.org/wiki/Bilinear_interpolation
# FB36 - 20130222
import random
import math
from PIL import Image, ImageDraw
from pprint import pprint
import os
from time import time
from itertools import product
t=time()
st=0
#imgx,imgy=(910,540)
imgx,imgy=(2000,2000)
neighbourhood={(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)}
far_neighbourhood = {(-4, -4), (-4, -3), (-4, -2), (-4, -1), (-4, 0), (-4, 1), (-4, 2), (-4, 3), (-4, 4), (-3, -4), (-3, -3), (-3, -2), (-3, -1), (-3, 0), (-3, 1), (-3, 2), (-3, 3), (-3, 4), (-2, -4), (-2, -3), (-2, -2), (-2, -1), (-2, 0), (-2, 1), (-2, 2), (-2, 3), (-2, 4), (-1, -4), (-1, -3), (-1, -2), (-1, -1), (-1, 0), (-1, 1), (-1, 2), (-1, 3), (-1, 4), (0, -4), (0, -3), (0, -2), (0, -1), (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (1, -4), (1, -3), (1, -2), (1, -1), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2, -4), (2, -3), (2, -2), (2, -1), (2, 0), (2, 1), (2, 2), (2, 3), (2, 4), (3, -4), (3, -3), (3, -2), (3, -1), (3, 0), (3, 1), (3, 2), (3, 3), (3, 4), (4, -4), (4, -3), (4, -2), (4, -1), (4, 0), (4, 1), (4, 2), (4, 3), (4, 4)}
precaculated_neighbourhood={}
urbanised_cells=set()
cells_for_urbanisation=set()
fresh_cells=set()
def create_image():
    image = Image.new("HSV", (imgx, imgy))
    draw = ImageDraw.Draw(image)
    pixels = image.load()
    octaves = int(math.log(max(imgx, imgy), 2.0))
    persistence = 0.9
    imgAr = [[0.0 for i in range(imgx)] for j in range(imgy)] # image array
    totAmp = 0.1
    for k in range(octaves):
        freq = 2 ** k
        amp = persistence ** k
        totAmp += amp
        # create an image from n by m grid of random numbers (w/ amplitude)
        # using Bilinear Interpolation
        n = freq + 1; m = freq + 1 # grid size
        ar = [[random.random() * amp for i in range(n)] for j in range(m)]
        nx = imgx / (n - 1.0); ny = imgy / (m - 1.0)
        for ky in range(imgy):
            for kx in range(imgx):
                i = int(kx / nx); j = int(ky / ny)
                dx0 = kx - i * nx; dx1 = nx - dx0
                dy0 = ky - j * ny; dy1 = ny - dy0
                z = ar[j][i] * dx1 * dy1
                z += ar[j][i + 1] * dx0 * dy1
                z += ar[j + 1][i] * dx1 * dy0
                z += ar[j + 1][i + 1] * dx0 * dy0
                z /= nx * ny
                imgAr[ky][kx] += z # add image layers together

    # paint image
    mult=1.25+random.random()*0.3
    for ky in range(imgy):
        for kx in range(imgx):
            c = imgAr[ky][kx] / totAmp *mult+0.02*random.random()
            pixels[kx, ky] = (max(int(120*(1-c)),0), 180, 120)
    return image

def spontangenous_growth_prob(height):
    ratio=height**3/1728000
    return ratio*0.2

def diffusive_growth_prob(height):
    ratio=height**5/24883200000
    return ratio*1.

def step():
    global t
    global st
    new_time=time()
    print(st,new_time-t)
    t=new_time
    st+=1


def SLEUTH_step(im):
    global st
    st=0
    step()
    pixels=im.load()
    imgx,imgy=im.size
    #pprint([[pixels[x,y] for y in range(imgy)] for x in range(imgx)])
    global fresh_cells
    global cells_for_urbanisation
    global precaculated_neighbourhood
    step()
    for x,y in fresh_cells:
        pixels[x,y]=(188,0,0)
        cells_for_urbanisation.add((x,y))
        urbanised_cells.add((x,y))
    #cells_for_urbanisation = {(x,y) for x,y in cells_for_urbanisation if len(precaculated_neighbourhood[(x,y)] & urbanised_cells)<8
    #                            and len(precaculated_far_neighbourhood[(x,y)] & urbanised_cells)<0.8*len(precaculated_far_neighbourhood[(x,y)])}
    #cells_for_urbanisation = {(x,y) for x,y in cells_for_urbanisation if len(precaculated_neighbourhood[(x,y)] & urbanised_cells)<8
    #                            and len(precaculated_far_neighbourhood[(x,y)] & urbanised_cells)<0.6*len(precaculated_far_neighbourhood[(x,y)])}
    step()
    cells_for_urbanisation = {(x,y) for x,y in cells_for_urbanisation if len(calculate_neighbourhood(x,y) & urbanised_cells)<8}
    for x,y in urbanised_cells - cells_for_urbanisation:
        if (x,y) in precaculated_neighbourhood:
            del precaculated_neighbourhood[(x,y)]
    fresh_cells=set()
    #spontangenous growth
    for i in range(int(3*max(2,int(0.0001*imgx*imgy)-len(urbanised_cells)))):
        x=random.randint(0,imgx-1)
        y=random.randint(0,imgy-1)
        if pixels[x,y][0]!=188:
            if spontangenous_growth_prob(pixels[x,y][0])>random.random():
                pixels[x,y]=(188,256,256)
                fresh_cells.add((x,y))
    #diffusive growth
    step()
    for x,y in cells_for_urbanisation:
        #fn=precaculated_far_neighbourhood[(x,y)]
        #population_density = len(fn & (urbanised_cells))/len(fn)
        #if random.random()<=(12/7-(15/7)*population_density):
        #if random.random()<=(2-(10/3)*population_density):
        for i,j in calculate_neighbourhood(x,y):
            #if len(calculate_neighbourhood(i,j) & urbanised_cells)>5:
            #    pixels[x,y]=(188,256,256)
            #    fresh_cells.add((i,j))
            if pixels[i,j][0]!=188 and diffusive_growth_prob(pixels[i,j][0])*max(1,len(calculate_neighbourhood(i,j) & urbanised_cells)/3-1/3)>random.random():
                pixels[x,y]=(188,256,256)
                fresh_cells.add((i,j))
    step()        
    for x,y in fresh_cells:
        pixels[x,y]=(188,256,256)
    for x,y in urbanised_cells:
        pixels[x,y]=(188,0,0)
    for x,y in cells_for_urbanisation:
        pixels[x,y]=(188,256,128)
    print(len(cells_for_urbanisation),len(urbanised_cells),len(cells_for_urbanisation)/max(1,len(urbanised_cells)))
    step()      
#for i in range(20,25):
#    image=create_image()
#    image=image.convert('RGB')
#    print('image done')
#    image.save("E:\\noise\\PerlinNoise{}.png".format(i), "PNG")

def calculate_neighbourhood(x,y):
    if (x,y) in precaculated_neighbourhood:
        return precaculated_neighbourhood[(x,y)]
    precaculated_neighbourhood[(x,y)]=set((x-a,y-b) for a,b in neighbourhood if 0<=(x-a)<imgx and 0<=(y-b)<imgy)
    return set((x-a,y-b) for a,b in neighbourhood if 0<=(x-a)<imgx and 0<=(y-b)<imgy)

import os
it=list(product(range(imgx),range(imgy)))
#precaculated_neighbourhood = {(x,y):set(((x-a + imgx )% imgx,(y-b + imgy )% imgy) for a,b in neighbourhood if 0<=(x-a)<imgx and 0<=(y-b)<imgy) for x,y in it}
#precaculated_far_neighbourhood = {(x,y):{((x-a + imgx )% imgx,(y-b + imgy )% imgy) for a,b in far_neighbourhood if 0<=(x-a)<imgx and 0<=(y-b)<imgy} for x,y in product(range(imgx),range(imgy))}
print('precalculation finished')
for i in range(1):
    urbanised_cells=set()
    cells_for_urbanisation=set()
    fresh_cells=set()
    #image=create_image()
    image=Image.open("E:\\city\\terrain3\\{}.png".format(i))
    image=image.convert('HSV')
    for j in range(8000):
        try:
            os.makedirs('E:\\city\\{}'.format(i))
        except:
            pass
        SLEUTH_step(image)
        print(i,j)
        image_to_save=image.convert('RGB')
        image_to_save.save("E:\\city\\{0}\\SLEUTH{1:0>5}.png".format(i,j), "PNG")
